

## Зависимости

```bash
pip install APScheduler
pip install dramatiq
pip install dramatiq-abort
pip install requests
pip install sqlmodel
pip install "uvicorn[standard]"
pip install fastapi
```

## Для запуска
* Убедится в установленном локально Redis и доступности его по адресу 127.0.0.1:6379

Или запустить docker контейнер redis
```
docker run --name my-redis -p 6379:6379 -d redis
```
* ** 

* В файле credentials.py указать TOKEN для доступа к сервису рассылки

## Запуск
Корневой каталог app

Запустить fastapi
```
python main.py
```

Запустить dramatiq

```
dramatiq background.dram
```


## Использование

Документация к API по адресу 
```
http://0.0.0.0:8000/docs
```

