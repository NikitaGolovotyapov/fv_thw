from typing import Optional
import re

from fastapi import HTTPException, status
from sqlmodel import SQLModel, Field, Column, VARCHAR
from pydantic import validator

from datetime import datetime
from enum import Enum


class Status(str, Enum):
    done = 'Complete'
    error = 'Error'


class Filter(SQLModel):
    mobile_operator_code: Optional[str] = ''
    tag: Optional[str] = ''

    class Config:
        schema_extra = {
            "example": {
                "mobile_operator_code": "XXX",
                "tag": "string",
            }
        }

    def is_exists(self):
        return self.mobile_operator_code or self.tag


class NewsletterCreate(SQLModel):
    time_stamp_begin_at: datetime
    text: str
    filter: Filter
    time_stamp_end_at: datetime

    @validator('time_stamp_end_at')
    def validate_time_stamp_end_at(cls, v, values): # noqa
        if v is None:
            return v
        if v <= values['time_stamp_begin_at']:
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                                detail="The end time must be longer than the start time")
        return v


class NewsletterUpdate(SQLModel):
    id: int
    time_stamp_begin_at: Optional[datetime]
    text: str
    filter: Filter
    time_stamp_end_at: Optional[datetime]


class NewsletterDelete(SQLModel):
    id: int


class Newsletter(SQLModel, table=True):
    __tablename__ = 'newsletter'
    id: int = Field(primary_key=True)
    time_stamp_begin_at: datetime = Field()
    text: str = Field()
    filter: Optional[str] = Field()
    time_stamp_end_at: Optional[datetime] = Field()


class ClientCreate(SQLModel):
    phone_number: str = Field()
    mobile_operator_code: str = Field()
    tag: str = Field()
    timezone: str = Field()

    class Config:
        schema_extra = {
            "example": {
                "phone_number": "7XXXXXXXXXX",
                "mobile_operator_code": "XXX",
                "tag": 'string',
                "timezone": '+2.5',
            }
        }

    @validator('phone_number')
    def validate_pn(cls, v): # noqa
        if v is None:
            return v
        if not re.fullmatch(r'^7\d{10}$', v):
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                                detail="The phone number must be in the format 7XXXXXXXXXX")
        return v

    @validator('mobile_operator_code')
    def validate_moc(cls, v): # noqa
        if v is None:
            return v
        if not re.fullmatch(r'^\d{3}$', v):
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                                detail="The mobile operator code must be in the format XXX")
        return v

    @validator('timezone')
    def validate_tz(cls, v): # noqa
        if v is None:
            return v
        try:
            tz = float(v)
        except ValueError:
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                                detail="The timezone must be in the format +-24")
        if tz >= 24.0 or tz <= -24.0:
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                                detail="The timezone must be in the range (-24 .. +24) ")
        return v


class ClientUpdate(ClientCreate):
    id: int
    phone_number: Optional[str]
    mobile_operator_code: Optional[str]
    tag: Optional[str]
    timezone: Optional[str]

    class Config:
        schema_extra = {
            "example": {
                "id": 0,
                "phone_number": "7XXXXXXXXXX",
                "mobile_operator_code": "XXX",
                "tag": 'string',
                "timezone": '+2',
            }
        }


class ClientDelete(SQLModel):
    id: int


class Client(SQLModel, table=True):
    __tablename__ = 'client'
    id: int = Field(primary_key=True)
    phone_number: str = Field(sa_column=Column("phone_number", VARCHAR, unique=True))
    mobile_operator_code: str = Field()
    tag: str = Field()
    timezone: str = Field()


class Message(SQLModel, table=True):
    __tablename__ = 'message'
    id: int = Field(primary_key=True)
    time_stamp_sending_at: datetime = Field(default_factory=lambda: datetime.utcnow())
    status: Status
    id_newsletter: int = Field(foreign_key='newsletter.id')
    id_client: int = Field(foreign_key='client.id')


class Task(SQLModel, table=True):
    id: int = Field(primary_key=True)
    id_newsletter: int = Field(foreign_key='newsletter.id')
    schedul_start_job_id: Optional[str]
    schedul_stop_job_id: Optional[str]
    dramatiq_job_id: Optional[str]

    def __repr__(self):
        return f"Task {self.id:=} ,{self.id_newsletter:=}, {self.schedul_start_job_id:=}, " \
               f"{self.schedul_stop_job_id:=}, {self.dramatiq_job_id}"

