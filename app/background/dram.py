import json

import requests
import dramatiq

from sqlmodel import select, Session
from dramatiq.brokers.redis import RedisBroker
import dramatiq_abort.backends
from dramatiq_abort import Abortable
from dramatiq_abort.middleware import Abort

from database.database import get_session
from models.models import Client, Newsletter, Message, Status
from credentials import TOKEN


URL = 'https://probe.fbrq.cloud/v1/send/'

redis_broker = RedisBroker(host='127.0.0.1', port=6379)
dramatiq.set_broker(redis_broker)

abortable = Abortable(
    backend=dramatiq_abort.backends.RedisBackend(client=redis_broker.client)
)
dramatiq.get_broker().add_middleware(abortable)


@dramatiq.actor
def run_newsletter(id_newsletter):
    session: Session = next(get_session())
    db_newsletter = session.exec(select(Newsletter).where(Newsletter.id == id_newsletter)).first()
    filters = json.loads(db_newsletter.filter) if db_newsletter.filter else None
    match filters:
        case None:
            db_clients = session.exec(select(Client)).all()
        case {'tag': a, "mobile_operator_code": b} if a and b:
            db_clients = session.exec(select(Client).where(Client.tag == filters['tag']).
                                      where(Client.mobile_operator_code==filters['mobile_operator_code'])).all()
        case {'tag': a, "mobile_operator_code": b} if a and not b:
            db_clients = session.exec(select(Client).where(Client.tag == filters['tag'])).all()
        case {'tag': a, "mobile_operator_code": b} if not a and b:
            db_clients = session.exec(select(Client).
                                      where(Client.mobile_operator_code == filters['mobile_operator_code'])).all()
    try:
        for id_element, client in enumerate(db_clients):
            response = requests.post(f"{URL}{id_element}",
                                     headers={
                                         "accept": "application/json",
                                         "Authorization": f"Bearer {TOKEN}"
                                     },
                                     json={
                                         "id": id_element,
                                         "phone": client.phone_number,
                                         "text": db_newsletter.text
                                     }
                                     )
            if not response.ok:
                db_message = Message(status=Status.error, id_newsletter=db_newsletter.id, id_client=client.id)
                session.add(db_message)
                session.commit()
                raise ConnectionError
            db_message = Message(status=Status.done, id_newsletter=db_newsletter.id, id_client=client.id)
            session.add(db_message)
            session.commit()
    except Abort as abr:
        pass
