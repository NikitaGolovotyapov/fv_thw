from dramatiq_abort import abort
from sqlmodel import Session, select

from database.database import get_session
from aps.aps import scheduler
from models.models import Task, Newsletter
from background.dram import run_newsletter


def start_dramatiq_task(id_newsletter):
    session: Session = next(get_session())
    message = run_newsletter.send_with_options(args=(id_newsletter,))
    db_task = session.exec(select(Task).where(Task.id_newsletter == id_newsletter)).first()
    db_task.dramatiq_job_id = message.message_id
    session.add(db_task)
    session.commit()
    session.refresh(db_task)


def stop_dramatiq_task(id_newsletter):
    session: Session = next(get_session())
    db_task = session.exec(select(Task).where(Task.id_newsletter == id_newsletter)).first()
    abort(db_task.dramatiq_job_id)


def f_task_new(newsletter: Newsletter):
    session: Session = next(get_session())
    db_task = Task(id_newsletter=newsletter.id)
    session.add(db_task)
    session.commit()
    aps_job_start = scheduler.add_job(start_dramatiq_task, 'date', run_date=newsletter.time_stamp_begin_at,
                                      args=[newsletter.id])
    aps_job_stop = scheduler.add_job(stop_dramatiq_task, 'date', run_date=newsletter.time_stamp_end_at,
                                     args=[newsletter.id])
    db_task = session.exec(select(Task).where(Task.id_newsletter == newsletter.id)).first()
    db_task.schedul_start_job_id = aps_job_start.id
    db_task.schedul_stop_job_id = aps_job_stop.id
    session.add(db_task)
    session.commit()
    session.refresh(db_task)
    session.close()


def f_task_update(newsletter: Newsletter):
    session: Session = next(get_session())
    db_task = session.exec(select(Task).where(Task.id_newsletter == newsletter.id)).first()
    job_start = scheduler.get_job(db_task.schedul_start_job_id)
    job_stop = scheduler.get_job(db_task.schedul_stop_job_id)
    if job_start.next_run_time != newsletter.time_stamp_begin_at:
        job_start.remove()
        aps_job_start = scheduler.add_job(start_dramatiq_task, 'date', run_date=newsletter.time_stamp_begin_at,
                                          args=[newsletter.id])
        db_task.schedul_start_job_id = aps_job_start.id
    if job_stop.next_run_time != newsletter.time_stamp_end_at:
        job_stop.remove()
        aps_job_stop = scheduler.add_job(stop_dramatiq_task, 'date', run_date=newsletter.time_stamp_end_at,
                                         args=[])
        db_task.schedul_stop_job_id = aps_job_stop.id
    session.add(db_task)
    session.commit()
    session.refresh(db_task)
    session.close()


def f_task_delete(newsletter: Newsletter):
    session: Session = next(get_session())
    db_task = session.exec(select(Task).where(Task.id_newsletter == newsletter.id)).first()
    job_start = scheduler.get_job(db_task.schedul_start_job_id)
    job_stop = scheduler.get_job(db_task.schedul_stop_job_id)
    job_start.remove()
    job_stop.remove()
    if db_task.dramatiq_job_id:
        abort(db_task.dramatiq_job_id)
    session.delete(db_task)
    session.commit()
