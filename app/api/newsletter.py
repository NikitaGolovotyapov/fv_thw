from fastapi import APIRouter, status, Depends, BackgroundTasks

from models.models import Newsletter, NewsletterCreate, NewsletterUpdate, NewsletterDelete
from repositories.newsletter import NewsletterRepository
from background.tasks import f_task_new, f_task_update, f_task_delete
router = APIRouter()


@router.post('/', response_model=Newsletter)
def add_newsletter(newsletter: NewsletterCreate, background_tasks: BackgroundTasks,
                   repository: NewsletterRepository = Depends(NewsletterRepository)):
    db_newsletter = repository.add_newsletter(newsletter)
    background_tasks.add_task(f_task_new, db_newsletter)
    return db_newsletter


@router.put('/', response_model=Newsletter)
def update_newsletter(client: NewsletterUpdate, background_tasks: BackgroundTasks,
                      repository: NewsletterRepository = Depends(NewsletterRepository)):
    db_newsletter = repository.update_newsletter(client)
    background_tasks.add_task(f_task_update, db_newsletter)
    return db_newsletter


@router.delete('/', status_code=status.HTTP_200_OK)
def delete_newsletter(client: NewsletterDelete, background_tasks: BackgroundTasks,
                      repository: NewsletterRepository = Depends(NewsletterRepository)):
    response = repository.delete_newsletter(client)
    background_tasks.add_task(f_task_delete, NewsletterDelete.id)
    return response
