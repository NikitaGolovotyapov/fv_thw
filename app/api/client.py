from fastapi import APIRouter, status, Depends

from models.models import Client, ClientCreate, ClientUpdate, ClientDelete
from repositories.client import ClientRepository

router = APIRouter()


@router.post('/', response_model=Client)
def add_client(client: ClientCreate, repository: ClientRepository = Depends(ClientRepository)):
    return repository.add_client(client)


@router.put('/', response_model=Client)
def update_client(client: ClientUpdate, repository: ClientRepository = Depends(ClientRepository)):
    return repository.update_client(client)


@router.delete('/', status_code=status.HTTP_200_OK)
def delete_client(client: ClientDelete, repository: ClientRepository = Depends(ClientRepository)):
    return repository.delete_client(client)
