from fastapi import APIRouter
from prometheus_client import Counter, Histogram
import time
import random

router = APIRouter()

REDIRECT_COUNT = Counter("redirect_total", "Count of redirects", ["redirected_from", "status"])
# REDIRECT_COUNT = Counter("redirect_total", "Count of redirects")
REDIRECT_HISTOGRAM = Histogram("redirect__duration_seconds", "Some_View duration, in seconds")


# @REDIRECT_HISTOGRAM.time()
def some_view(ok: bool):
    time1 = random.randint(1, 100)/100
    time.sleep(time1)
    REDIRECT_HISTOGRAM.observe(time1)
    if ok:
        REDIRECT_COUNT.labels("some_view", 'ok').inc()
    else:
        REDIRECT_COUNT.labels("some_view", 'no ok').inc()
    return True
