import logging
import time

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import uvicorn

from starlette_exporter import PrometheusMiddleware, handle_metrics

from database.database import create_db_and_tables
from api.client import router as client_router
from api.newsletter import router as newsletter_router

from aps.aps import scheduler

app = FastAPI()


logging.basicConfig(filename='app.log', format='%(asctime)s - %(message)s', level=logging.INFO)
logging.Formatter.converter = time.gmtime

app.include_router(client_router, prefix="/client", tags=['client'])
app.include_router(newsletter_router, prefix="/newsletter", tags=['newsletter'])
app.add_middleware(PrometheusMiddleware, app_name="hello_world", prefix='myapp')
app.add_route("/metrics", handle_metrics)
# Enable CORS
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.on_event("startup")
def on_startup():
    scheduler.start()
    create_db_and_tables()


@app.on_event("shutdown")
def shutdown_event():
    scheduler.shutdown()


if __name__ == '__main__':
    uvicorn.run(app, host="0.0.0.0", port=8000)  # noqa
