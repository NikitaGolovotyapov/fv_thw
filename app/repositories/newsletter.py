from fastapi import Depends, status, HTTPException
from sqlmodel import select

from database.database import Session, get_session
from models.models import Newsletter, NewsletterCreate, NewsletterUpdate, NewsletterDelete


class NewsletterRepository:
    def __init__(self, session: Session = Depends(get_session)):
        self.session = session

    def add_newsletter(self, newsletter: NewsletterCreate):
        db_newsletter = Newsletter(**newsletter.dict())
        if newsletter.filter.is_exists():
            db_newsletter.filter = newsletter.filter.json()
        self.session.add(db_newsletter)
        self.session.commit()
        self.session.refresh(db_newsletter)
        return db_newsletter

    def get_newsletter_by_id(self, newsletter_id):
        return self.session.exec(select(Newsletter).where(Newsletter.id == newsletter_id)).first()

    def update_newsletter(self, newsletter: NewsletterUpdate):
        db_newsletter = self.get_newsletter_by_id(newsletter.id)
        if not db_newsletter:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                                detail="Newsletter id not found")
        newsletter_update_data = newsletter.dict(exclude_unset=True, exclude={'filter'})
        for key, value in newsletter_update_data.items():
            setattr(db_newsletter, key, value)
        db_newsletter.filter = newsletter.filter.json() if newsletter.filter.is_exists() else None
        self.session.add(db_newsletter)
        self.session.commit()
        self.session.refresh(db_newsletter)
        return db_newsletter

    def delete_newsletter(self, newsletter: NewsletterDelete):
        db_newsletter = self.get_newsletter_by_id(newsletter.id)
        if not db_newsletter:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                                detail="Newsletter id not found")
        self.session.delete(db_newsletter)
        self.session.commit()
        return {'ok': True}
