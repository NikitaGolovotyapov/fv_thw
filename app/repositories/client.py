from fastapi import Depends, status, HTTPException
from sqlmodel import select

from database.database import Session, get_session
from models.models import Client, ClientCreate, ClientUpdate, ClientDelete
from repositories.base import BaseRepository


class ClientRepository(BaseRepository):
    def __init__(self, session: Session = Depends(get_session)):
        super().__init__(session)

    def get_client_by_id(self, client_id) -> Client:
        return self.session.exec(select(Client).where(Client.id == client_id)).first()

    def add_client(self, client: ClientCreate) -> Client:
        db_client = Client(**client.dict())
        return self.add_to_db(db_client)

    def update_client(self, client: ClientUpdate) -> Client:
        db_client = self.get_client_by_id(client.id)
        if not db_client:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                                detail="Client id not found")
        client_update_data = client.dict(exclude_unset=True)
        for key, value in client_update_data.items():
            setattr(db_client, key, value)
        return self.add_to_db(db_client)

    def delete_client(self, client: ClientDelete) -> dict:
        db_client = self.get_client_by_id(client.id)
        if not db_client:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                                detail="Client id not found")
        self.delete_from_db(db_client)
        return {'ok': True}
