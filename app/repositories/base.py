from sqlmodel import Session
from sqlalchemy.exc import IntegrityError
from fastapi import HTTPException, status


class BaseRepository:
    def __init__(self, session: Session):
        self.session = session

    def add_to_db(self, orm_obj):
        try:
            self.session.add(orm_obj)
            self.session.commit()
            self.session.refresh(orm_obj)
            return orm_obj
        except IntegrityError as ie:
            raise HTTPException(status_code=status.HTTP_409_CONFLICT,
                                detail=ie.args)

    def delete_from_db(self, orm_obj):
        try:
            self.session.delete(orm_obj)
            self.session.commit()
        except IntegrityError as ie:
            raise HTTPException(status_code=status.HTTP_409_CONFLICT,
                                detail=ie.args)
